<?php
/**
 *   Plugin Name: Nss automatic order management
 *   Plugin URI:
 *   Description: Automatic order processing
 *   Author: Green friends
 *   Author URI:
 *   Version: 0.3
 */

// Prevent direct access
defined('ABSPATH') || die();

//require 'vendor/autoload.php';
//require ('classes/NSS_Log.php');
require ('classes/NSS_Backorder.php');
require ('classes/NSS_BankReport.php');
require ('classes/NSS_CourierReport.php');
require ('classes/NSS_Setup.php');

register_activation_hook(__FILE__, ['NSS_Setup', 'nss_install']);
register_deactivation_hook(__FILE__, ['NSS_Setup', 'nss_uninstall']);
//register_uninstall_hook(    __FILE__, 'mrp_uninstall' );

add_action('admin_menu', function (){
    add_submenu_page('nss-panel', 'Obrada porudžbenica', 'Obrada porudžbenica', 'edit_pages', 'nss-orders', 'getTabs', 1);
});

function getTabs(){
    global $wpdb;
    include 'html/myHeader.php';

    $route = isset($_GET['tab']) ? $_GET['tab'] : 'bankReportForm';
    switch ($route){
        case 'bankReportForm':
            include 'html/bankReportForm.php';
            break;

        case 'bankReportAction':
            $bankReport = new NSS_BankReport();
            if (isset($_POST['orderId'])) {
                if (isset($_POST['selected'])) {
                    $unresolvedPayments = $bankReport->changeOrdersStatusByIdFromPost();
                } else {
                    $unresolvedPayments = [];
                    echo 'nista nije izabrano.';
                }
            } else {
                $unresolvedPayments = $bankReport->changeOrdersStatusByIdFromBankReport();
            }
            include('html/bankReportList.php');

            break;

        case 'courierReportForm':
            include 'html/courierReportForm.php';
            break;

        case 'courierReportAction':
            $courierReport = new NSS_CourierReport();
            $courierReport->changeOrderStatusByIdFromCourierReport();

            break;

        case 'backOrderCandidates':
            $arg = array('orderby' => 'date', 'status' => ['u-pripremiplaceno', 'u-pripremi'], 'posts_per_page' => '500');
            $orders = WC_get_orders($arg);

            include 'html/backOrderCandidates.php';
            break;

        case 'backOrderManualCreate':
            $orders = [];
            foreach ($_POST['orderId'] as $orderId) {
                $order = wc_get_order($orderId);
                $orders[] = $order;
            }

            $backorders = new NSS_Backorder($wpdb);
            $backorders->createBackOrders($orders);
            include 'html/backOrderManualCreate.php';

            break;

        case 'backOrderDelete':
            $backorders = new NSS_Backorder($wpdb);
            $backorders->deleteBackOrder($_GET['id']);
            echo 'Obrisan nalog: ' . $_GET['id'];

            break;

        case 'backOrderList':
            $backorders = new NSS_Backorder($wpdb);
            $backorders = $backorders->getBackOrders();
            include 'html/backOrderList.php';

            break;

        //@TODO output required html only, avoid wp stuff
        case 'backOrderCopy':
            $backorder = new NSS_Backorder($wpdb);
            $backorders = $backorder->getBackOrders($_GET['id']);
            /* @var WP_User $supplier */
            $supplier = get_user_by('id', $backorders[0]->supplierId);
            header('Content-Type: text/html; charset=utf-8');
            header('Content-Disposition: attachment;filename="'.$supplier->display_name.' - '.$_GET['id'].'-'.date('dmy').'.html"');

            include 'html/backOrderProcess.php';

            break;

        case 'backOrderEmail':
            $backorder = new NSS_Backorder($wpdb);
            $backorders = $backorder->getBackOrders($_GET['id']);
            $supplierId = $backorders[0]->supplierId;
            if ($backorder->sendBackOrderEmail($supplierId, $backorders)) {
                echo 'mail sent';
            }

            break;

        case 'backOrderProcess':
            $backorders = new NSS_Backorder($wpdb);
            if (isset($_POST['submit'])) {
                $backorders->processItems($_GET['id'], $_POST['itemId']);
            }
            $backorders = $backorders->getBackOrders($_GET['id']);

            include 'html/backOrderProcess.php';

            break;

        case 'itemExport':
            ini_set('memory_limit', '512M');
            ini_set('max_execution_time', '300');
        $csv = '';
        for ($i=1; $i<9; $i++) {
            $args     = array(
                'post_type' => 'product',
//                'category' => 34,
                'posts_per_page' => 4000,
                'page' => $i,
                'status' => 'publish'
            );
            $products = wc_get_products($args);

            /* @var $product WC_Product_Simple|WC_Product_Variable */
            foreach($products as $product) {
                if($product->get_meta('pdv') >= 10) {
                    $taxcalc = (int) ('1' . $product->get_meta('pdv'));
                } else {
                    $taxcalc = (int) ('10' . (int) $product->get_meta('pdv'));
                }

                $csv .= @iconv('utf-8','windows-1250',  $product->get_sku()."\t".trim(mb_strtoupper($product->get_name(), 'UTF-8'))."\t".
                        str_replace('.', ',', $product->get_meta('pdv'))."\t".str_replace('.', ',', round($product->get_price() * 100 / (double) $taxcalc, 2))."\t".
                        str_replace('.', ',', round($product->get_price(), 2)))."\r\n";

                if (get_class($product) === WC_Product_Variable::class) {
                    $passedIds = [];
                    foreach ($product->get_available_variations() as $variations) {
                        foreach ($variations['attributes'] as $variation) {
                            $itemIdSize = $product->get_sku() . $variation;
                            if (!in_array($itemIdSize, $passedIds)) {
                                $passedIds[] = $itemIdSize;
                                $csv .= iconv('utf-8','windows-1250',  $itemIdSize."\t".
                                trim(mb_strtoupper($product->get_name() . ' ' . $variation, 'UTF-8'))."\t".
                                str_replace('.',',',$product->get_meta('pdv'))."\t".str_replace('.', ',', round($product->get_price() * 100 / (double) $taxcalc, 2))."\t".
                                str_replace('.', ',', round($product->get_price(), 2)))."\r\n";
//                                var_dump($product->get_sku() . $variation);
                            }
                        }
                    }
                }
            }
        }

//            header("Cache-Control: public");
//            header("Content-Description: File Transfer");
//            header('Content-type: text/plain');
//            header("Content-Disposition: attachment; filename=".date('d-m-Y H:i:s').'.txt');
//            header('Content-Transfer-Encoding: binary');

            echo $csv;
            die();

            break;

        case 'getDailyExport':


            break;

        case 'createDailyExport':
//            mkdir(ABSPATH . '/wp-content/uploads/adresnice/');
            if (isset($_POST['orderId'])) {
                createDailyExport($_POST['orderId']);
                header('Location: ' . home_url() . '/back-ajax/?action=getAdresnice');
            } else {
                $arg = array('orderby' => 'date', 'status' => ['spz-slanje'], 'posts_per_page' => '500');
                $orders = WC_get_orders($arg);
                include 'html/createDailyExport.php';
            }

            break;

        default:
            include 'html/bankReportForm.php';
            break;
    }
}

add_filter('woocommerce_order_number', 'nss_woocommerce_order_number', 1, 2);
/**
 * Add Prefix to WooCommerce Order Number
 */
function nss_woocommerce_order_number($oldnumber, WC_Order $order) {
    if ($order->get_date_created()) {
        return $order->get_date_created()->date('dmY') .'-'. $order->get_id();
    }
    $dt = new \DateTime('now', new \DateTimeZone('Europe/Belgrade'));

    return $dt->format('dmY') .'-'. $order->get_id();

//    $dateCreated = date('dmY', strtotime($order->get_date_created()));
//    return $dateCreated .'-'. $order->get_id();
}

function createDailyExport($orderIds) {
    $adresnicaFields = [
        'ReferenceID','SBranchID','SName','SAddress','STownID','STown','SCName','SCPhone','PuBranchID','PuName',
        'PuAddress','PuTownID','PuTown','PuCName','PuCPhone','RBranchID','RName','RAddress','RTownID','RTown','RCName',
        'RCPhone','DlTypeID','PaymentBy','PaymentType','BuyOut','Value','Mass','ReturnDoc','SMS_Sender','Packages','Note',
        'Content', 'BuyOutFor', 'BuyOutAccount'
    ];

    $fileName = 'adresnica-' . date('dmy') . '.csv';
    $adresnicaPath = ABSPATH . '/wp-content/uploads/adresnice/' . $fileName;
    $tmpFile = fopen($adresnicaPath, 'wa');

    //insert csv header
    if (!fputcsv($tmpFile, $adresnicaFields, '|')) {
        throw new Exception('Doslo je do greske prilikom generisanja csv izvestaja.');
    }

    /* @var \WC_Order $order */
    foreach ($orderIds as $orderId) {
        $order = wc_get_order($orderId);
        $billingName = $order->get_billing_first_name() .' '. $order->get_billing_last_name();
        if ($order->get_shipping_address_1() !== ''):
            $shippingName = $order->get_shipping_first_name() .' '. $order->get_shipping_last_name();
            $shippingAddress = $order->get_shipping_address_1();
            $shippingZip = $order->get_shipping_postcode();
            $shippingCity = $order->get_shipping_city();
        else:
            $shippingName = $order->get_billing_first_name() .' '. $order->get_billing_last_name();
            $shippingAddress = $order->get_billing_address_1();
            $shippingZip = $order->get_billing_postcode();
            $shippingCity = $order->get_billing_city();
        endif;
        $otkupnina = 0;
        $buyOutFor = -1;
        $buyOutAccount = '';
        if ($order->get_payment_method_title() === 'Pouzećem') {
            $otkupnina = number_format($order->get_total(), 0, '', '');
            $otkupnina = $otkupnina * 100;
            $buyOutFor = 0;
            $buyOutAccount = '160-487203-63';
        }
        $weight = 0;
        $category = '';

        /* @var WC_Order_Item_Product $item */
        foreach ($order->get_items() as $item) {
            $weight += $item->get_product()->get_weight();
            if (isset($item->get_product()->get_category_ids()[0])) {
                $cat = get_term_by('id', $item->get_product()->get_category_ids()[0], 'product_cat');
                $category = $cat->name;
            }
        }
        if ($category == '') {
            $category = 'Pokloni';
        }

        // i.e. 2018 -> '' , 2012 -> ''
        // explode, and search date string to strip only in first part
//        $dailyCode = 'SS'.preg_replace('/2018/', '', str_replace('-', '', $order->get_order_number()), 1);
//        $dailyCode = preg_replace('/2019/', '', $dailyCode);
        $codeLength = strlen($order->get_order_number());
        $dailyCode = 'SS' . preg_replace('/2019/', '', str_replace('-', '', $order->get_order_number()), 1);
//        if (strlen($dailyCode) === $codeLength) {
        $dailyCode = preg_replace('/2021/', '', $dailyCode, 1);
        $dailyCode = preg_replace('/2022/', '', $dailyCode, 1);
//        }

        $csvArray = array(
            'gpoid' => $order->get_order_number(),

            'ID nalogodavca' => 'UK17357',
            'Naziv nalogodavca' => 'Non Stop Shop d.o.o.',
            'Adresa nalogodavca' => 'Gvozdićeva 4',
            'ID naselja nalogodavca' => 11000,
            'Naziv naselja/mesta nalogodavca' => 'Beograd',
            'Kontakt osoba nalogodavca' => 'NonStopShop',
            'Kontakt telefon nalogodavca' => '011/7450-380',

            'ID posaljioca' => 'UK17357',
            'Naziv posaljioca' => 'Non Stop Shop d.o.o.',
            'Adresa mesta preuzimanja' => 'Gvozdićeva 4',
            'ID naselja mesta preuzimanja' => 11000,
            'Naziv naselja/mesta preuzimanja' => 'Beograd',
            'Kontakt osoba mesta preuzimanja' => 'NonStopShop',
            'Kontakt telefon mesta preuzimanja' => '011/7450-380',

            'ID primaoca' => '',
            'Naziv primaoca' => $shippingName,
            'Adresa primaoca' => $shippingAddress,
            'ID naselja primaoca' => $shippingZip,
            'Naziv naselja/mesta primaoca' => $shippingCity,
            'Kontakt osoba primaoca' => $billingName,
            'Kontakt telefon primaoca' => $order->get_billing_phone(),

            'Tip isporuke' => 2,
            'Ko plaća' => 1,
            'Način plaćanja' => 2,
            'Otkupnina' => $otkupnina,
            'Vrednost' => 0,
            'Masa' => $weight * 1000,
            'Povratna dokumentacija' => 0,
            'SMS pošiljaoca' => '',
            'Paketi' => $dailyCode,
            'Napomena' => '',
            'Content' => $category,
            'BuyOutFor' => $buyOutFor,
            'BuyOutAccount' => $buyOutAccount
        );

        if (!fputcsv($tmpFile, $csvArray, '|')) {
            die('error saving csv');
        }
        $order->update_status('poslato');
    }
}