<div class="wrap">
    <div class="Header">
        <h1>Obrada porudzbenica</h1>
        <nav class="nav-tab-wrapper woo-nav-tab-wrapper">
            <a href="/wp-admin/admin.php?page=nss-orders&tab=backOrderList" class="<?php if (isset($_GET['tab']) && $_GET['tab'] == 'backOrderList'){echo 'nav-tab nav-tab-active';}else{echo 'nav-tab';} ?> ">Pregledaj naloge</a>
            <a href="/wp-admin/admin.php?page=nss-orders&tab=backOrderCandidates" class="<?php if (isset($_GET['tab']) && $_GET['tab'] == 'backOrderManualCreate'){echo 'nav-tab nav-tab-active';}else{echo 'nav-tab';} ?> ">Kreiraj naloge</a>
            <a href="/wp-admin/admin.php?page=nss-orders&tab=bankReportForm" class="<?php if (isset($_GET['tab']) && $_GET['tab'] == 'bankReportForm'){echo 'nav-tab nav-tab-active';}else{echo 'nav-tab';} ?>">Evidentiranje uplata</a>
            <a href="/wp-admin/admin.php?page=nss-orders&tab=courierReportForm" class="<?php if (isset($_GET['tab']) && $_GET['tab'] == 'courierReportForm'){echo 'nav-tab nav-tab-active';}else{echo 'nav-tab';} ?> ">Isporučene pošiljke</a>
            <a href="/wp-admin/admin.php?page=nss-orders&tab=createDailyExport" class="<?php if (isset($_GET['tab']) && $_GET['tab'] == 'createDailyExport'){echo 'nav-tab nav-tab-active';}else{echo 'nav-tab';} ?> ">Poslate pošiljke</a>
            <a target="_blank" href="/back-ajax/?action=jitexItemExport" class="<?php if (isset($_GET['tab']) && $_GET['tab'] == 'itemExport'){echo 'nav-tab nav-tab-active';}else{echo 'nav-tab';} ?> ">Export proizvoda za jitex</a>
        </nav>
    </div>
</div>