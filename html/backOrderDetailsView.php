<?php
/**
 * @var WP_User $supplier
 */

$text .= '<table class="widefat fixed" cellspacing="0" border="1">
    <tr>
<!--        <th>Izaberi</th>-->
        <th>Šifra NSS</th>
        <th>Šifra proizvoda</th>
        <th>Naziv proizvoda</th>
        <th>Veličina</th>
        <th>MP Cena (kom)</th>
        <th>Poreska osnovica (kom)</th>
        <th>Stopa PDV-a</th>
        <th>Naručeno komada</th>
        <th>Ima na stanju</th>
        <th>Ukupno za naručivanje</th>
        <th>Broj porudzbine</th>
        <th>Način placanja</th>';
if (!$print) {
    $text .= '<th>Sravni</th>';
}

$text .= '</tr>';

foreach ($backorders as $order):
    $pdv = str_replace('.00', '', $order->pdv);
    $item = wc_get_product($order->itemId);
    $wcOrder = wc_get_order($order->orderId);
//    $orderQty = abs($order->qty - $item->get_meta('quantity'));
//    if ($orderQty < 0) {
//        $orderQty = 0;
//    }
    $style = '';
    $input = 'checked';
    if (!$print) {
        if ($order->itemStatus) {
            $style = 'style="background-color:gray"';
            $input = 'disabled';
        }
        if ($wcOrder->get_status() == 'stornirano') {
            $style = 'style="background-color:#ff6961"';
            $input = 'disabled';
        }
        if ($order->itemStatus != 0) {
            $style = 'style="opacity:0.3"';
        }
    }

    $text .= '<tr '.$style.'>
            <td>'.$item->get_sku().'</td>
            <td>'.$item->get_meta('vendor_code').'</td>
            <td>'.$order->name.'</td>
            <td>'.$order->variant.'</td>
            <td>'.$order->price .'</td>
            <td>'.round($order->price * 100 / (100 + $pdv), 2) .'</td>
            <td>'.$order->pdv.'</td>
            <td>'.$order->qty.'</td>
            <td>'.(int) $item->get_meta('quantity').'</td>
            <td>'.$order->totalQty.'</td>
            <td>'. $wcOrder->get_order_number().'</td>
            <td>'.$wcOrder->get_payment_method_title().'</td>';
    if (!$print) {
        $text .= '<td>';
        $checkedT = $checkedF = $checked0 = '';
        if ($order->itemStatus === 1) {
            $checkedT = ' checked ';
        }
        if ($order->itemStatus === -1) {
            $checkedF = ' checked ';
        }
        if ($order->itemStatus == 0) {
            $checked0 = ' checked ';
        }
            $text .= '<p><input type="radio" name="itemId[' . $wcOrder->get_id() . '][' . $order->itemId . '#' . $order->variant . ']" value="1" '.$checkedT.' /> Sravni <br />
                <input type="radio" name="itemId[' . $wcOrder->get_id() . '][' . $order->itemId . '#' . $order->variant . ']" value="0" '.$checked0.' /> čeka se <br />
                <input type="radio" name="itemId[' . $wcOrder->get_id() . '][' . $order->itemId . '#' . $order->variant . ']" value="-1" '.$checkedF.' /> nema na stanju </p>';
//        }
        $text .= '</td>';
    }
    $text .= '</tr>';
endforeach;
$text .= '</table>';