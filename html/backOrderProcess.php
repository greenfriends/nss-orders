<?php
/* @var WP_User $supplier */
$supplier = get_user_by('id', $backorders[0]->supplierId);
$supplierData = get_user_meta($backorders[0]->supplierId);
?>
<script>
    function printDiv() {
        var w = window.open();
        w.document.write(document.getElementById('content-print').innerHTML);
        w.print();
        w.close();
    }
</script>

<h1>Pregled naloga</h1>

<?php if ($backorders[0]->orderStatus == 4): ?>
<h2>Nalog kompletiran</h2>
<?php endif; ?>
<p>Broj naloga: <?= $backorders[0]->backOrderId ?></p>
<p>Dobavljač: <?= $supplier->display_name ?> (id: <?= $supplier->ID ?>)</p>
<p>Podaci o dobavljaču: <?= $supplierData['vendor_phone'][0] . ' ' . $supplierData['vendor_address'][0] ?></p>
<p>Datum kreiranja: <?= $backorders[0]->createdAt ?></p>
<!--<p>Status: </p>-->

<div class="mb-3">
    <button onclick="printDiv()" class="print button button-primary mr-2">Print</button>
    <a href="admin.php?page=nss-orders&tab=backOrderEmail&id=<?= $backorders[0]->backOrderId ?>"
       class="button button-primary mr-2">Email</a>
    <a href="admin.php?page=nss-orders&tab=backOrderCopy&id=<?= $backorders[0]->backOrderId ?>"
       class="button button-primary">Kopija</a>
    <!--<a href="admin.php?page=nss-orders&tab=backOrderExport&id=<?= $backorders[0]->backOrderId ?>">!Eksport</a> -->
</div>

<form method="post" action="">
    <?php
    $text = '';
    $print = false;
    include('backOrderDetailsView.php');
    echo $text;
    ?>

    <?php if ($backorders[0]->orderStatus > 1 && $backorders[0]->orderStatus < 4): ?>
        <input type="submit" name="submit" value="Sravni nalog"/>
    <?php endif; ?>
    <?php if ($backorders[0]->orderStatus == 4): ?>
        <h2>Nalog kompletiran</h2>
    <?php endif; ?>
</form>

<div id="content-print" style="display: none">
    <?php
    $print = true;
    $text = '';
    include('backOrderPrint.php');
    //    echo $text;
    ?>
</div>