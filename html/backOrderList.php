<?php

if (is_admin()) {
    new Gf_Back_Order_Wp_List_Table($backorders);
}

/**
 * Paulund_Wp_List_Table class will create the page to load the table
 */
class Gf_Back_Order_Wp_List_Table
{
    /**
     * Gf_Search_Wp_List_Table constructor.
     *
     */
    public function __construct($backorders)
    {
        $this->list_table_page($backorders);
    }

    /**
     * Display the list table page
     *
     * @return Void
     */
    public function list_table_page($backorders)
    {
        $ListTable = new Back_Order_Wp_List_Table($backorders);
        $ListTable->prepare_items();
        $table_data = $ListTable->table_data($backorders);
        $status_array = [];
        foreach ($table_data as $data) {

            if (in_array($data['status'], $status_array)) {
                continue;
            } else {
                $status_array[] = $data['status'];
            }

        }
        $dates_array = [];
        foreach ($table_data as $data) {

            if (in_array($data['createdAt'], $dates_array)) {
                continue;
            } else {
                $dates_array[] = $data['createdAt'];
            }

        }
        ?>
        <div class="wrap">
            <div id="icon-users" class="icon32"></div>
            <h2>Pregled naloga</h2>
            <form method="get" class="gf-backorders-filters">
                <input type="hidden" name="page" value="<?= $_REQUEST['page']?>"/>
                <input type="hidden" name="tab" value="<?= $_REQUEST['tab']?>"/>

                <?php $ListTable->search_box('search', 'search_id'); ?>

                <div class="row">
                    <label for="backorder_select_status">Izaberite status</label>
                    <select name="backorder_select_status" id="backorder_select_status">
                        <option value="">- Svi statusi -</option>
                        <?php foreach ($status_array as $status) {
                            echo '<option value="' . $status . '">' . $status . '</option>';
                        }
                        ?>
                    </select>
                </div>
                <div class="row">
                    <label for="backorder_select_date_from">Datum od</label>
                    <select name="backorder_select_date_from" id="backorder_select_date_from">
                        <option value="">- Svi datumi -</option>
                        <?php foreach ($dates_array as $date) {
                            echo '<option value="' . $date . '">' . $date . '</option>';
                        }
                        ?>
                    </select>

                    <label for="backorder_select_date_to">Datum do</label>
                    <select name="backorder_select_date_to" id="backorder_select_date_to">
                        <option value="">- Svi datumi -</option>
                        <?php foreach ($dates_array as $date) {
                            echo '<option value="' . $date . '">' . $date . '</option>';
                        }
                        ?>
                    </select>
                </div>
            </form>
            <?php $ListTable->display(); ?>
        </div>
        <?php
    }
}

if (!class_exists('WP_List_Table')) {
    require_once(ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
}

class Back_Order_Wp_List_Table extends WP_List_Table
{
    private $backorders;

    /** Class constructor */
    public function __construct($backorders)
    {
        parent::__construct([
            'singular' => 'Backorder',
            'plural' => 'Backorders',
            'ajax' => false //should this table support ajax?
        ]);
        $this->backorders = $backorders;
    }

    public function filter_search_table_data($table_data, $search_key)
    {
        $filtered_table_data = array_values(array_filter($table_data, function ($row) use ($search_key) {
            foreach ($row as $key => $row_val) {
                if($key == 'backOrderId' || $key == 'supplier'){
                    if (stripos($row_val, $search_key) !== false) {
                        return true;
                    }
                }
            }
        }));
        return $filtered_table_data;
    }

    public function filter_status_table_data($table_data, $search_key)
    {
        $filtered_table_data = array_values(array_filter($table_data, function ($row) use ($search_key) {
            foreach ($row as $key => $row_val) {
                if($key == 'status') {
                    if ($row_val == $search_key) {
                        return true;
                    }
                }
            }
        }));
        return $filtered_table_data;
    }

    public function filter_date_table_data($data, $date_from, $date_to)
    {
        $filtered_table_data = array_values(array_filter($data, function ($row) use ($date_from, $date_to) {
//@TODO srediti datum za obrnutu situaciju, ako je $date_from veci od $date_to
//            foreach ($row as $row_val) {
//                if ($date_from < $date_to) {
//                    if ($row_val >= $date_from && $row_val <= $date_to) {
//                        return true;
//                    }
//                } else {
//                    if ($row_val <= $date_from && $row_val >= $date_to) {
//                        return true;
//                    }
//                }
//            }

            foreach ($row as $key => $row_val) {
                if($key == 'createdAt'){
                    if ($row_val >= $date_from && $row_val <= $date_to) {
                        return true;
                    }
                }
            }


        }));
        return $filtered_table_data;
    }

    public function prepare_items()
    {
        $data = $this->table_data($this->backorders);

        if (isset($_GET['s']) && !empty($_GET['s'])) {
            $search_key = trim($_GET['s']);
            if ($search_key !== '') {
                $data = $this->filter_search_table_data($data, $search_key);
            }
        }

        if (isset($_GET['backorder_select_status']) && !empty($_GET['backorder_select_status'])) {
            $search_key = trim($_GET['backorder_select_status']);
            if ($search_key !== '') {
                $data = $this->filter_status_table_data($data, $search_key);
            }
        }

        if ((isset($_GET['backorder_select_date_from']) && !empty($_GET['backorder_select_date_from'])) && (isset($_GET['backorder_select_date_to']) && !empty($_GET['backorder_select_date_to']))) {
            $date_from = $_GET['backorder_select_date_from'];
            $date_to = $_GET['backorder_select_date_to'];
            $data = $this->filter_date_table_data($data, $date_from, $date_to);
        }


        $columns = $this->get_columns();
        $hidden = $this->get_hidden_columns();
        $sortable = $this->get_sortable_columns();
//        usort($data, array(&$this, 'sort_data'));
        $perPage = 30;
        $currentPage = $this->get_pagenum();
        $totalItems = count($data);
        $this->set_pagination_args(array(
            'total_items' => $totalItems,
            'per_page' => $perPage
        ));
        $data = array_slice($data, (($currentPage - 1) * $perPage), $perPage);
        $this->_column_headers = array($columns, $hidden, $sortable);
        $this->items = $data;
    }

    public
    function get_columns()
    {
        $columns = array(
            'backOrderId' => 'Broj naloga',
            'supplier' => 'Dobavljač',
            'createdAt' => 'Datum kreiranja',
            'status' => 'Status',
            'action' => 'Akcija',
        );
        return $columns;
    }

    public
    function get_hidden_columns()
    {
        return array();
    }

    public
    function get_sortable_columns()
    {
        return array(
            'backOrderId' => array('backOrderId', false),
            'supplier' => array('supplier', false),
            'createdAt' => array('createdAt', false),
            'status' => array('status', false),

        );
    }

    public
    function table_data($backorders)
    {
        $data = [];
        foreach ($backorders as $order) {
            $supplier = get_user_by('id', $order->supplierId);

            if ($order->status == 1): $status = 'novi';
            elseif ($order->status == 2): $status = 'naručen';
            elseif ($order->status == 3): $status = 'parcijalno izvršen';
            else: $status = 'izvršen'; endif;

            if ($order->status == 1):
                $action = '<a href="/wp-admin/admin.php?page=nss-orders&tab=backOrderProcess&id=' . $order->backOrderId . '">Pregledaj</a> | 
                            <a href="/wp-admin/admin.php?page=nss-orders&tab=backOrderDelete&id=' . $order->backOrderId . '">Obriši</a>';
            elseif ($order->status == 2 || $order->status == 3):
                $action = '<a href="/wp-admin/admin.php?page=nss-orders&tab=backOrderProcess&id=' . $order->backOrderId . '">Sravni</a>';
            else:
                $action = '<a href="/wp-admin/admin.php?page=nss-orders&tab=backOrderProcess&id=' . $order->backOrderId . '">Pregledaj</a>';
            endif;
            if ($order->mailSent == 0) {
                $action .= ' | <a href="/wp-admin/admin.php?page=nss-orders&tab=backOrderEmail&id=' . $order->backOrderId . '">Email</a>';
            }

            $data[] = array(
                'backOrderId' => $order->backOrderId,
                'supplier' => $supplier->display_name,
                'createdAt' => date("d-m-Y", strtotime($order->createdAt)),
                'status' => $status,
                'action' => $action,
            );
        }
        return $data;
    }


    public
    function column_default($item, $column_name)
    {
        switch ($column_name) {
            case 'backOrderId':
            case 'supplier':
            case 'createdAt':
            case 'status':
            case 'action':
                return $item[$column_name];
            default:
                return print_r($item, true);
        }
    }

    private
    function sort_data($a, $b)
    {
        // Set defaults
        $orderby = 'createdAt';
        $order = 'desc';
        // If orderby is set, use this as the sort column
        if (!empty($_GET['orderby'])) {
            $orderby = $_GET['orderby'];
        }
        // If order is set use this as the order
        if (!empty($_GET['order'])) {
            $order = $_GET['order'];
        }
        $result = strcmp($a[$orderby], $b[$orderby]);
        if ($order === 'asc') {
            return $result;
        }
        return -$result;

    }
}


if (isset($_POST['backOrdersManualCreate'])) {
    echo '<div class="notice notice-success is-dismissible">
        <p>Uspešno ste kreirali naloge!</p>
    </div>';
}

?>