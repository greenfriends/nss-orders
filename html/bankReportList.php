<?php if (count($unresolvedPayments)): ?>
<form method="post" action="admin.php?page=nss-orders&tab=bankReportAction">
    <table>
        <tr>
            <th width="50">Ručno povezivanje (Match)</th>
            <th>Broj porudzbenice</th>
            <th>Ime</th>
            <th>Izvor</th>
            <th>Iznos</th>
            <th>Svrha</th>
            <th>Poziv na broj</th>
        </tr>
        <?php foreach ($unresolvedPayments as $key => $line): ?>
        <tr>
            <td><input type="checkbox" name="selected[<?=$key?>]" /> </td>
            <td><input name="orderId[<?=$key?>]" value="<?=$line[24]?>" /></td>
            <td><?=$line[2]?></td>
            <td><?=$line[6]?></td>
            <td><?=$line[12]?></td>
            <td><?=$line[20]?></td>
            <td><?=$line[24]?></td>
        </tr>
    <?php endforeach;?>
    </table>
    <input type="submit" value="Send" />
</form>
<?php endif; ?>