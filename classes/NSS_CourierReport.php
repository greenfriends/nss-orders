<?php

class NSS_CourierReport
{

    public function changeOrderStatusByIdFromCourierReport()
    {
        $data = $this->parseCourierReportData();
        $unresolvedData = [];
        foreach ($data as $line) {
            try {
                if (!strstr($line[1], 'SS')) {
                    continue;
                }
                $stripped = str_replace('SS', '', $line[1]);
                $orderId = substr($stripped, 4, strlen($stripped) - 4);
                $order = wc_get_order($orderId);
                if (!$order) {
                    $unresolvedData['notFound'][] = $line[0];
                    echo '<p>Porudzbenica '.$line[1].' nije pronadjena</p>';
                    continue;
//                    throw new Exception('Porudžbina ID ' . $line[0] . ' nije pronađena' . '</br>');
                }
                $orderStatus = $order->get_status();

                if ($orderStatus === 'poslato') {
//                    echo '<p>'.$line[1].' ready to update</p>';
                    $order->update_status('isporuceno');
                    $order->save();
                } else {
//                    echo '<p>'.$line[1].' has bad status: '.$orderStatus.'</p>';
                    $unresolvedData['badStatus'][] = $line[0];
                }
            } catch (\Exception $e) {
                echo $e->getMessage();
            }
        }

        return $unresolvedData;
    }

    /**
     * @return array
     */
    protected function parseCourierReportData()
    {
        $data = $this->readCourierReportFile();
        $worksheet = $data->getActiveSheet()
            ->rangeToArray(
                'A2:F' . $data->getActiveSheet()->getHighestRow(),
                null,
                true,
                true,
                false
            );

        return $worksheet;
    }

    protected function readCourierReportFile()
    {
        move_uploaded_file($_FILES['courierReportFile']['tmp_name'], '/tmp/tmp.xlsx');
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        $reader->setReadDataOnly(true);
        $data = $reader->load('/tmp/tmp.xlsx');

        return $data;
    }
}