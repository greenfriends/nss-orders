<?php

function set_html_content_type() { return 'text/html'; }

class NSS_Backorder
{
    /**
     * @var wpdb $wpdb
     */
    private $db;

    public static $unChangableStatuses = [
        'poslato', 'stornirano', 'finalizovano', 'stornirano-pn', 'vracena-posiljka', 'reklamacija', 'reklamacija-pnns'
    ];

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function getBackOrders($id = null)
    {
        $tableA = $this->db->prefix . NSS_Setup::BACKORDER_TABLE;
        $tableB = $this->db->prefix . NSS_Setup::BACKORDER_ITEMS_TABLE;

        if ($id) {
            $sql = "SELECT *, a.status as orderStatus, b.status as itemStatus FROM {$tableA} a JOIN {$tableB} b USING (backOrderId) 
            WHERE a.backOrderId = {$id} ORDER BY name";
        } else {
            $sql = "SELECT * FROM {$tableA} ORDER BY createdAt DESC";
        }

        return $this->db->get_results($sql);
    }

    public function getNewBackOrders($id = null)
    {
        $tableA = $this->db->prefix . NSS_Setup::BACKORDER_TABLE;
        $tableB = $this->db->prefix . NSS_Setup::BACKORDER_ITEMS_TABLE;

        if ($id) {
            $sql = "SELECT *, a.status as orderStatus, b.status as itemStatus FROM {$tableA} a JOIN {$tableB} b USING (backOrderId) 
          WHERE a.backOrderId = {$id}";
        } else {
            $sql = "SELECT * FROM {$tableA} WHERE status = 1";
        }

        return $this->db->get_results($sql);
    }

    public function deleteBackOrder($backOrderId)
    {
//        $sql = "SELECT orderId FROM " . $this->db->prefix . NSS_Setup::BACKORDER_ITEMS_TABLE . " WHERE backOrderId = {$backOrderId}";
//        foreach ($this->db->get_results($sql) as $orderId) {
//            $order = wc_get_order($orderId->orderId);
//            if ($order->get_status() === 'naruceno') {
//                $order->update_meta_data('backOrderUndo', true);
//                $order->set_status('u-pripremi');
//                $order->save();
//            }
//        }
//        $sql = "DELETE FROM " . $this->db->prefix . NSS_Setup::BACKORDER_TABLE . " WHERE backOrderId = {$backOrderId}";
//        $this->db->query($sql);
//        $sql = "DELETE FROM " . $this->db->prefix . NSS_Setup::BACKORDER_ITEMS_TABLE . " WHERE backOrderId = {$backOrderId}";
//        $this->db->query($sql);
    }

    /**
     * Should be fired via cron operation
     *
     * @throws Exception
     */
    public function createBackOrders($orders = null)
    {
        if (!$orders) {
            $arg = array('orderby' => 'date', 'status' => ['u-pripremiplaceno', 'u-pripremi'], 'posts_per_page' => '500');
            $orders = WC_get_orders($arg);
        }
        $data = [];
        //sort by vendors
        foreach ($orders as $order) {
             if ($order->get_meta('marketplaceVendor',true) !== ''){
               continue;
            }
            /** @var WC_Order_Item_Product $item */
            foreach ($order->get_items() as $item) {
                $product = $item->get_product();
                if (!$product) {
                    var_dump('Product not found: ' . $item->get_product_id());
//                    throw new Exception('Product not found: ' . $item->get_product_id());
                    continue;
                }
                $itemData = [
                    'productId' => $item->get_product_id(),
                    'vendorCode' => $item->get_meta('vendor_code'),
                    'qty' => $item->get_quantity(),
                    'totalQty' => $item->get_quantity(),
                    'name' => $item->get_name(),
                    'price' => $item->get_total() / $item->get_quantity(),
                    'pdv' => $product->get_meta('pdv'),
                    'orderId' => $order->get_id(),
                    'variant' => ''
                ];
                if ($product instanceOf WC_Product_Variation && !empty($product->get_variation_attributes())) {
                    $itemData['variant'] = implode(',', array_values($product->get_variation_attributes()));
                }
                $supplier_id = $product->get_meta('supplier');
                if ($supplier_id === '') {
                    $supplier_id = get_post_meta($item->get_product_id())['supplier'][0];
                }

                $data[$supplier_id][] = $itemData;
            }
            if (!in_array($order->get_status(), static::$unChangableStatuses)) {
                $order->update_status('naruceno');
            }

        }
        $tableA = $this->db->prefix . NSS_Setup::BACKORDER_TABLE;
        $tableB = $this->db->prefix . NSS_Setup::BACKORDER_ITEMS_TABLE;

        try {
            foreach ($data as $suppId => $items) {
                $backorderId_sql = "SELECT backOrderId FROM {$tableA} WHERE supplierId = {$suppId} AND status = 1";
                $result = $this->db->get_results($backorderId_sql);
                if (!empty($result)) {
                    //existing backorder
                    $backOrderId = $result[0]->backOrderId;
                    // loop tru items and add new ones
                    foreach ($items as $item) {
                        $item_sql = "SELECT * FROM {$tableB} WHERE itemId = {$item['productId']} AND backOrderId = {$backOrderId}";
                        $itemData = $this->db->get_results($item_sql);
                        if (empty($itemData)) {
                            // new item
                            $this->db->insert(
                                $this->db->prefix . NSS_Setup::BACKORDER_ITEMS_TABLE,
                                array(
                                    'name' => $item['name'],
                                    'itemId' => $item['productId'],
                                    'qty' => $item['qty'],
                                    'totalQty' => $item['totalQty'],
                                    'price' => $item['price'],
                                    'pdv' => $item['pdv'],
                                    'orderId' => $item['orderId'],
                                    'backOrderId' => $backOrderId,
                                    'variant' => $item['variant'],
                                )
                            );
                        } else {
                            // update item
                            $sql_variant = "SELECT variant FROM {$tableB} WHERE itemId LIKE {$item['productId']} AND backOrderId = {$backOrderId}";
                            $variant_result = $this->db->get_results($sql_variant)[0]->variant;
                            $sql_qty = "SELECT qty FROM {$tableB} WHERE itemId LIKE {$item['productId']}";
                            $old_qty = $this->db->get_results($sql_qty)[0]->qty;
                            $new_qty = (int) $old_qty + (int) $item['qty'];
                            //if same variant update qty only
                            $update_sql = "UPDATE {$tableB} SET qty = {$new_qty}, totalQty = {$new_qty} WHERE itemId LIKE {$item['productId']} AND backOrderId = {$backOrderId}";
                            if ($variant_result == $item['variant']) {
                                $this->db->query($update_sql);
                            } else {
                                $this->db->insert(
                                    $this->db->prefix . NSS_Setup::BACKORDER_ITEMS_TABLE,
                                    array(
                                        'name' => $item['name'],
                                        'itemId' => $item['productId'],
                                        'qty' => $item['qty'],
                                        'totalQty' => $item['totalQty'],
                                        'price' => $item['price'],
                                        'pdv' => $item['pdv'],
                                        'orderId' => $item['orderId'],
                                        'backOrderId' => $backOrderId,
                                        'variant' => $item['variant'],
                                    )
                                );
                            }
                        }
                    }
                } else {
                    $backOrderId = $this->createBackOrder($suppId, $items);
                    $this->autoProcessItems($backOrderId);
                }
            }
        } catch (Exception $e) {
            var_dump($e->getMessage());
            die();
        }
    }

    private function autoProcessItems($backOrderId)
    {
        $itemsTable = $this->db->prefix . NSS_Setup::BACKORDER_ITEMS_TABLE;

        // collect touched orders to check if its status needs an update
        $orders = [];

        //find items, and reduce local quantity first if applicable
        $sql = "SELECT * FROM {$itemsTable} WHERE backOrderId = {$backOrderId} AND status = 0";
        $items = $this->db->get_results($sql);
        foreach ($items as $item) {
            $product = wc_get_product($item->itemId);
            if ((int) $product->get_meta('quantity') > 0) {
                $qty = 0;
                $totalQty = 0;
                $itemStatus = 1;
                if ((int) $product->get_meta('quantity') >= (int) $item->qty) {
                    $qty = (int) $product->get_meta('quantity') - (int) $item->qty;
                } else {
                    //needs back order
                    $totalQty = (int) $item->qty - (int) $product->get_meta('quantity');
                    $itemStatus = 0;
                }
                update_post_meta($item->itemId, 'quantity', $qty);
                $sql = "UPDATE {$itemsTable} SET status = {$itemStatus}, totalQty = {$totalQty} WHERE orderId = {$item->orderId} 
                  AND backOrderId = {$backOrderId} AND itemId = {$item->itemId}";
                $this->db->query($sql);

                // check if backorder is auto completed
                $sql = "SELECT COUNT(*) as count FROM {$itemsTable} WHERE backOrderId = {$backOrderId} and status = 0";
                $itemCount = (int)$this->db->get_results($sql)[0]->count;
                $status = 3;
                //all items processed, update back order status
                if ($itemCount === 0) {
                    $status = 4;
                }
                $update = $this->db->update(
                    $this->db->prefix . NSS_Setup::BACKORDER_TABLE,
                    ['status' => $status],
                    ['backOrderId' => $backOrderId]
                );
                if (!$update && $this->db->last_error != '') {
                    throw new Exception($this->db->last_error);
                }
                $orders[$item->orderId] = $item->orderId;
            }
        }

        //update statuses for complete shipments for orders
        foreach ($orders as $orderId) {
            $order = wc_get_order($orderId);
            if (in_array($order->get_status(), static::$unChangableStatuses)) {
                continue;
            }
            // all items arrived
            $sql = "SELECT COUNT(*) as waitingItemsCount FROM {$itemsTable} WHERE orderId = {$orderId} AND status <> 1 AND totalQty > 0;";
            if ((int) $this->db->get_results($sql)[0]->waitingItemsCount === 0) {
                if (!in_array($order->get_status(), static::$unChangableStatuses)) {
                    $order->update_status('spz-pakovanje');
                    $order->save();
                }
            }
            // item not available
//            $sql = "SELECT COUNT(*) as waitingItemsCount FROM {$itemsTable} WHERE orderId = {$orderId} AND status = -1;";
//            if ((int) $this->db->get_results($sql)[0]->waitingItemsCount > 0) {
//                if (!in_array($order->get_status(), static::$unChangableStatuses)) {
//                    $order->update_status('reklamacija-pnns');
//                    $order->save();
//                }
//            }
        }

        return true;
    }

    public function processItems($backOrderId, $data)
    {
        $itemsTable = $this->db->prefix . NSS_Setup::BACKORDER_ITEMS_TABLE;
        // collect touched orders to check if its status needs an update
        $orders = [];
        if ($data) {
            foreach ($data as $orderId => $itemData) {
                foreach ($itemData as $key => $status) {
                    $itemParams = explode('#', $key);
                    $itemId = $itemParams[0];
                    $orders[] = $orderId;
                    $this->db->show_errors(true);
                    $sql = "UPDATE `{$itemsTable}` SET status = {$status} WHERE orderId = {$orderId} 
                      AND backOrderId = {$backOrderId} AND itemId = {$itemId}";
                    if (isset($itemParams[1])) {
                        $sql .= " AND variant = '{$itemParams[1]}' ";
                    }
                    $update = $this->db->query($sql);
                    if ($update === false) {
                        var_dump($this->db->last_error);
                        var_dump($this->db->print_error());
                        die();
                    }
                }
            }
        }

        //update statuses for complete orders
        foreach ($orders as $orderId) {
            $order = wc_get_order($orderId);
            // all items arrived
            $sql = "SELECT COUNT(*) as waitingItemsCount FROM {$itemsTable} WHERE orderId = {$orderId} AND status <> 1 AND totalQty > 0;";
            if ((int) $this->db->get_results($sql)[0]->waitingItemsCount === 0) {
                if (!in_array($order->get_status(), static::$unChangableStatuses)) {
                    $order->update_status('spz-pakovanje');
                    $order->save();
                }
            }
            // item not available
            $sql = "SELECT COUNT(*) as waitingItemsCount FROM {$itemsTable} WHERE orderId = {$orderId} AND status = -1;";
            if ((int) $this->db->get_results($sql)[0]->waitingItemsCount > 0) {
                if (!in_array($order->get_status(), static::$unChangableStatuses)) {
                    $order->update_status('reklamacija-pnns');
                    $order->save();
                }
            }
        }

        $sql = "SELECT COUNT(*) as count FROM {$itemsTable} WHERE backOrderId = {$backOrderId} and status = 0";
        $itemCount = (int)$this->db->get_results($sql)[0]->count;
        $status = 3;
        //all items processed, update back order status
        if ($itemCount === 0) {
            $status = 4;
        }
        $update = $this->db->update(
            $this->db->prefix . NSS_Setup::BACKORDER_TABLE,
            ['status' => $status],
            ['backOrderId' => $backOrderId]
        );
        if (!$update && $this->db->last_error != '') {
            throw new Exception($this->db->last_error);
        }

        return true;
    }

    public function sendBackOrderEmail($supplierId, $backorders)
    {
        $supplier = get_user_by('id', $supplierId);
        if (!$supplier) {
            throw new Exception('Supplier not found: ' . $supplierId);
        }
        $text = '';
        $from = 'prodaja@nonstopshop.rs';
        $qtyForOrdering = 0;
        include(__DIR__ . '/../html/backOrderEmail.php');

        foreach (explode(',', $supplier->description) as $email) {
            $to[] = $email;
        }
        $to[] = 'prodaja@nonstopshop.rs';
//        $to = []; // debug
//        $to[] = 'djavolak@mail.ru';

        // fix from headers
        $headers = [
            'Content-Type: text/html; charset=UTF-8',
            "From: NonStopShop <{$from}>",
            "Reply-to: NonStopShop <{$from}>",
//            "Bcc: djavolak@mail.ru"
        ];
        $table = $this->db->prefix . NSS_Setup::BACKORDER_TABLE;

        //only send mail if there is actual quantity to be ordered
        if ($qtyForOrdering) {
            wp_mail($to, 'NonStopShop.rs - Narudžbenica', $text, $headers);
            remove_filter('wp_mail_content_type', 'set_html_content_type');
            $update_sql = "UPDATE {$table} SET mailSent = 1 WHERE backOrderId = {$backorders[0]->backOrderId}";
            $this->db->query($update_sql);

            if ($backorders[0]->status < 2) {
                $update_sql = "UPDATE {$table} SET status = 2 WHERE backOrderId = {$backorders[0]->backOrderId}";
                $this->db->query($update_sql);
            }
        }

        return (bool) $qtyForOrdering;
    }

    protected function createBackOrder($supplierId, $items, $status = 1)
    {
        $this->db->insert(
            $this->db->prefix . NSS_Setup::BACKORDER_TABLE,
            array(
                'supplierId' => $supplierId,
                'status' => $status,
            )
        );
        $backOrderId = $this->db->insert_id;
        // @TODO add default status for items
        foreach ($items as $item) {
            $this->db->insert(
                $this->db->prefix . NSS_Setup::BACKORDER_ITEMS_TABLE,
                array(
                    'name' => $item['name'],
                    'itemId' => $item['productId'],
                    'qty' => $item['qty'],
                    'totalQty' => $item['totalQty'],
                    'price' => $item['price'],
                    'pdv' => $item['pdv'],
                    'orderId' => $item['orderId'],
                    'backOrderId' => $backOrderId,
                    'variant' => $item['variant'],
                )
            );
        }

        return $backOrderId;
    }
}