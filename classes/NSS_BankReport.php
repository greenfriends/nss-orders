<?php

class NSS_BankReport
{
    public function changeOrdersStatusByIdFromPost()
    {
        $unresolvedPayments = [];
        $resolved = [];
        foreach ($_POST['selected'] as $orderId => $on) {
            /* @var WC_Order $order */
            $order = wc_get_order(trim(explode('-', $_POST['orderId'][$orderId])[1]));
            if (!$order) {
                $unresolvedPayments[] = $orderId;
                continue;
                throw new Exception('Porudžbina ID ' . $orderId . ' nije pronađena' . '</br>');
            }
            $this->changeOrderStatus($order);
            $resolved[] = $order;
        }
        echo sprintf('Izmenjeno ukupno %s porudžbina.', count($resolved));

        return $unresolvedPayments;
    }

    public function changeOrdersStatusByIdFromBankReport()
    {
        $data = $this->parseBankReportData();
        $unresolvedPayments = [];
        $resolved = [];
        foreach ($data as $line) {
            $cleanLine = str_replace('00 ', '', $line[24]);
            $cleanLine = str_replace(' ', '', $cleanLine);
            $orderId = trim(str_replace('\n', '', $cleanLine));
            try {
                //skip boring items, but display them
                if (!strstr($orderId, '-')) {
                    $unresolvedPayments[] = $line;
                    continue;
                }

                /* @var WC_Order $order */
                $order = wc_get_order(explode('-', $orderId)[1]);
                if (!$order) {
                    $unresolvedPayments[] = $line;
                    continue;
                }
                $formatTotal = number_format($order->get_total(), 2, '.', ',');
                if ($formatTotal === $line[12]) {
                    $this->changeOrderStatus($order);
                    $resolved[] = $order;
                } else {
                    $unresolvedPayments[] = $line;
                }
            } catch (\Exception $e) {
                echo $e->getMessage();
                $unresolvedPayments[] = $line;
            }
        }
        echo sprintf('Izmenjeno ukupno %s porudžbina.', count($resolved)) . PHP_EOL;
        echo sprintf('od ukupno %s pregledano.', count($data));

        return $unresolvedPayments;
    }

    protected function changeOrderStatus(WC_Order $order)
    {
        $orderStatus = $order->get_status();
        if ($orderStatus === 'finalizovano') {
            return true;
        }

        //uplatnicom ide u pripremi placeno
        if ($orderStatus === 'cekaseuplata') {
            $order->update_status('u-pripremiplaceno');
        //isporuceno ide u finalizovano
        } else if ($orderStatus === 'isporuceno') {
            $order->update_status('finalizovano');
            //pouzecem ide u completed
        } elseif ($order->get_payment_method() === 'cod') {
            $order->update_status('finalizovano');
        } else {
            return false;
        }
        $order->save();

        return true;
    }

    protected function parseBankReportData()
    {
        if (isset($_POST['orderId'])) {
            if (!isset($_POST['selected'])) {
                return [];
            }
            return $_POST['selected'];
        } else {
            $data = $this->readBankReportFile();
            $worksheet = $data->getActiveSheet()
                ->rangeToArray(
                    'A28:Y' . $data->getActiveSheet()->getHighestRow(),
                    null,
                    true,
                    true,
                    false
                );

            return $worksheet;
        }
    }

    protected function readBankReportFile()
    {
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls;
        $reader->setReadDataOnly(true);

        return $reader->load($_FILES['bankReportFile']['tmp_name']);
    }
}